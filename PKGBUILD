# Maintainer: Philip Müller <philm at manjaro dot org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Balló György <ballogyor+arch at gmail dot com>

pkgname=gnome-usage-mobile
pkgver=45.0
pkgrel=1
pkgdesc="View information about use of system resources"
arch=(aarch64)
url="https://wiki.gnome.org/Apps/Usage"
license=(GPL3)
depends=(
  glib2
  gtherm
  gtk4
  libadwaita
  libgee
  libgtop
  tracker3
  xdg-user-dirs
)
makedepends=(
  accountsservice
  gamemode
  git
  meson
  vala
)
optdepends=(
  'accountsservice: show user tags for processes'
  'gamemode: show processes requesting game mode'
)
provides=("gnome-usage=$pkgver")
conflicts=(gnome-usage)
_commit=3e3fb2ac7108a2fc54efd983080e34fc5964a742  # tags/45.0^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-usage.git#commit=$_commit"
        'Add-libgtherm-dependency.patch'
        'Add-initial-thermal-view.patch'
        'Make-the-headerbar-aware-of-the-thermal-page.patch')
b2sums=('SKIP'
        '00af3314b7ec300efef256a1c9299ec29e43644c5ecdf405752cf2fc6dee770579c13e31cdd2de81d529b54d62642f2aa37a1b105fdb041b463ec85579eec5b9'
        '5a6f46ee0b2021ceaa2a42e19cd66f70c396f0af1cf365d02202f5b4c69bf32d982cbb7b4cbf67e8e24fed17799efeb486f91705c3b74d836bc6a82966b43f10'
        'a6bef64b66b559d25a7608778107a2e5ccdfeb9697d815354b225d5f3644120198e3870423ede8e1d53885839cf19ebf36071795a4e754358ddcd8a08ff5b434')

pkgver() {
  cd gnome-usage
  git describe --tags | sed 's/^v//;s/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd gnome-usage

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    msg2 "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  arch-meson gnome-usage build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
  echo "X-Purism-FormFactor=Workstation;Mobile;" >> "$pkgdir/usr/share/applications/org.gnome.Usage.desktop"
}

# vim:set sw=2 sts=-1 et:

